# Gitlab für Texte

... in Schule, Studium und Wissenschaft!

## Warum und wie sieht das aus?

[Ein kurzes Video als Einführung](https://youtu.be/i8MxZU9w6bc)


## Hier gehts zum Kurs in 

* [HTML](https://axel-klinger.gitlab.io/gitlab-for-documents/index.html)
* [PDF](https://axel-klinger.gitlab.io/gitlab-for-documents/course.pdf)
* [EPUB](https://axel-klinger.gitlab.io/gitlab-for-documents/course.epub)
* [LiaScript](https://liascript.github.io/course/?https://axel-klinger.gitlab.io/gitlab-for-documents/course.md)
